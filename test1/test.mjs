import util from 'util';
import * as utils from 'test-utils';

const params = {password: 'sdf234fdsf32cdsc'};
const myPromise = util.promisify(utils.runMePlease);

try {
    const result = await myPromise(params);
    console.log(result)
} catch (e) {
    console.error(e)
}

